import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-projects-details',
  templateUrl: './projects-details.component.html',
  styleUrls: ['./projects-details.component.css']
})
export class ProjectsDetailsComponent implements OnInit {

  // buttonText: string;
  // actionMode: string;
  // @Input() selectedClientObj: any;

  projectObjForm: FormGroup;
  isShowLoading = false;


  constructor(private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private router: Router,
    // private location: Location
    // private apiBankHelperService: ApiBankHelperService
    ) {

    this.projectFormGroup();
    this.getProjectById();
  }

  ngOnInit() {

    // this.activatedRoute.params.subscribe(
    //   params => {
    //     this.clientoId = params.id;
    //     this.buttonText = this.clientoId != '0' ? 'Update' : 'Save';
    //     this.actionMode = this.clientoId != '0' ? OBJ_EDIT : OBJ_NEW;
    //   });

    //   if(this.actionMode == OBJ_EDIT){
    //     this.getClientById();
    //   }else{
    //     this.isShowLoading = false;
    //   }
  }

  projectFormGroup(){
    this.projectObjForm = this.fb.group({
      id: [''],
      title: ['',  [Validators.required]],
      details: ['',  [Validators.required]]
    });
  }



  getProjectById() {

  //   this.apiBankHelperService.getJWtToken().subscribe(res => {
  //     const token = res.data.accessToken;
  //     this.apiBankHelperService.getClientByOid(this.clientoId,token).subscribe(res => {
  //       this.clientData = res['data'];
  //       this.isShowLoading = false;
  //       this.populateData(res.data);
  //     });
  // });
  }


  populateData(myData:any) {
    this.projectObjForm.patchValue(myData);
  }

  save() {

  }




  get title() {
    return this.projectObjForm.get('title');
  }

  get details() {
    return this.projectObjForm.get('details');
  }

  // closeButtonAction() {
  //   this.location.back();
  // }

}
