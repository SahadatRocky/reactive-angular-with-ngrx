import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css']
})
export class ProjectsListComponent implements OnInit {

  isShowLoading = false;
  displayedColumns: string[];
  paginatorLength: number;
  dataSize = 10;
  public pageSizeOptions = [10,20,100,500];
  pageEvent: PageEvent;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  dataSource = new MatTableDataSource();
  constructor(
    private router: Router,
    public dialog: MatDialog
    // private apiBankHelperService: ApiBankHelperService
    ) { }

  ngOnInit() {
    this.getProjectList(0, this.dataSize);
    this.setColumns();
  }

  getProjectList(page: number, size: number) {

    // this.apiBankHelperService.getJWtToken().subscribe(res => {
    //     const token = res.data.accessToken;
    //     this.apiBankHelperService.getClientObjTableData(page,size,token).subscribe(res => {
    //       this.isShowLoading = false;
    //       console.log("----",res.data.content);
    //       this.setTableData(res.data.content);
    //       this.setPagination(res.data.totalPages, res.data.totalElements);
    //     });
    // });
  }

  setTableData(list: any) {
    this.dataSource.data = list;
  }

  setColumns() {
    this.displayedColumns = ['title', 'details' , 'action'];
  }


  applyFilter(filterValue: string) {

    //  if(filterValue.length >= 3){
    //   this.apiBankHelperService.getJWtToken().subscribe(res => {
    //     const token = res.data.accessToken;
    //     this.apiBankHelperService.getClientTableDataForSearch(0, this.dataSize, filterValue,token).subscribe(
    //       res => {
    //         this.isShowLoading = false;
    //         this.setDashboardData(res.data.content);
    //         this.setPagination(res.data.totalPages, res.data.totalElements);
    //       },
    //       error => console.log(error)
    //     );
    //   })
    // }else if(filterValue.length == 0){
    //   this.getClientList(0, this.dataSize);
    // }
  }

  createNewClientObj(){
    // this.router.navigate(['dashboard/client-edit/' + 0]);
  }

  onPaginateChange(pageEvent: PageEvent) {
    this.getProjectList(pageEvent.pageIndex, pageEvent.pageSize);
  }

  setPagination(totalPages: number, totalElements: number) {
    this.paginatorLength = totalElements;
  }

  removeApiBankRow(event:any) {
    console.log(event);
    // this.apiBankHelperService.getJWtToken().subscribe(res => {
    //   const token = res.data.accessToken;
    //   this.apiBankHelperService.removePresentationItemForAPIBank(event.oid,token).subscribe(
    //     res => {
    //       this.populateApiBankData(0, this.dataSize, this.apioId);

    //       this.util.showUpdateSuccess(this.dialog, res.data);
    //     }, error => {
    //       console.log(error);
    //       this.util.showUpdateUnsuccessful(this.dialog, error.error.message, error.error.generalErrors);
    //     });
    // })
  }


  // sendRowData(event:any) {
  //   console.log(event);
  //   this.router.navigate(['dashboard/client-edit/' + event.oid]);
  // }

  // sendRowClientInfoData(event:any){
  //   console.log(event);
  //   this.openClientInfoDialogue(event.oid);
  //   // ClientInfoComponent
  // }

  // openClientInfoDialogue(clientoid:string) {

  //     const dialogRef = this.dialog.open(ClientInfoComponent, {
  //       disableClose: true,
  //       width: '450px',
  //       height: '300px',
  //       data: {
  //         "clientOid": clientoid
  //       }
  //     });
  //     dialogRef.afterClosed().subscribe(
  //       result => {
  //       });
  //   }

}
